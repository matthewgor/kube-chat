# kube-chat

A simple group chat written in nodejs with isomorphic-ws


```mermaid 
erDiagram
          PostgreSQL }|..|{ SERVER : log  
          SERVER ||--o{ CLIENT1 : tx-rx
          SERVER ||--o{ CLIENT2 : tx-rx
```

Kubernetes technologies used in this project:

1. Multiple services
2. Utilizing secrets
3. Utilizing Configurations
4. Using volumes
5. Proof of concept used centralized logging
