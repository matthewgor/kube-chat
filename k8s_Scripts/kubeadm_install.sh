#!/usr/bin/bash

echo "--> Disabing swap"
swapoff -a

echo "--> attempting to remove old versions of docker"
apt remove docker docker-engine docker.io

echo "-->installing apt transports and curl..."
apt install -y apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

apt-key fingerprint 0EBFCD88

echo "--> adding apt repo for lsb stable..."
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update

echo "--> installing docker-ce- and containerd.io"
apt install -y docker-ce docker-ce-cli containerd.io

echo "--> more containerd.io"
apt-get update && apt-get install -y \
containerd.io=1.2.13-2 \
docker-ce=5:19.03.11~3-0~ubuntu-$(lsb_release -cs) \
docker-ce-cli=5:19.03.11~3-0~ubuntu-$(lsb_release -cs)

#echo "--> creating docker user"
#useradd docker -d /home/docker -m -g docker

#usermod -aG docker docker

echo "--> running modprobe br_netfilter..."
modprobe br_netfilter

echo "--> running docker hello world container..."
docker run hello-world

echo "--> setting up docker daemon..."
bash -c 'cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF'


mkdir -p /etc/systemd/system/docker.service.d

systemctl daemon-reload
systemctl restart docker


echo "--> setting up apt transport https curl..."
apt-get update && apt-get install -y apt-transport-https curl

echo "--> setting up kubernetes repo..."
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

bash -c "cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF"

echo "--> installing k8s code..."
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

echo "--> sanity checks for k8s..."
kubeadm version
kubelet --version
kubectl version

exit 0
